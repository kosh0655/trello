good_registration = (
    'Ivan Ivanov',
    'qwerty147874@ya.ru',
    '98741ytrewq',
    True,
    'https://trello.com/create-first-board'
)

already_exists = (
    'Ivan Ivanov',
    'qwerty147874@ya.ru',
    '98741ytrewq',
    True,
    'https://trello.com/signup'
)

empty_name = (
    '',
    'acdscc5cbc@ya.ru',
    'qwerdfdty',
    False,
    'https://trello.com/signup'
)

space_name = (
    ' ',
    'acdsc3ccbc@ya.ru',
    'qwerdfdty',
    True,
    'https://trello.com/signup'
)

long_name = (
    '~!@#$%^&*()_+QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>?QWERTYUIOP{}',
    'acdscc1cbc@ya.ru',
    'qwerdfdty',
    True,
    'https://trello.com/create-first-board'
)

sql_name = (
    'select * from users;',
    'ac1587@ya.ru',
    'qwerdfdty',
    True,
    'https://trello.com/create-first-board'
)

empty_email = (
    'sadad dsaasd',
    '',
    'qwerdfdty',
    False,
    'https://trello.com/signup'
)

space_email = (
    'sadad dsaasd',
    ' ',
    'qwerdfdty',
    False,
    'https://trello.com/signup'
)

long_email = (
    'sadad dsaasd',
    '~!@#$%^&*()_QWERTYUIOP{ASDFGHJKL:ZXCVBNM<@ya.ru',
    'qwerdfdty',
    True,
    'https://trello.com/signup'
)

empty_passw = (
    'sasa dasdas',
    'acd321scccbc@ya.ru',
    '',
    False,
    'https://trello.com/signup'
)

space_passw = (
    'sasa dasdas',
    'acdscc0@ya.ru',
    ' ',
    True,
    'https://trello.com/signup'
)

good_auth = (
    'qwerty147874@ya.ru',
    '98741ytrewq',
    'board'
)

empty_user = (
    '',
    '98741ytrewq',
    'login'
)

empty_passw_auth = (
    'qwerty1478@ya.ru',
    '',
    'login'
)
