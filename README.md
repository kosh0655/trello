**For test run:**
1. Install requirements
2. Type in terminal command:

    `pytest -s test_scenarios.py`
3. Look and have fun
4. You can change test data in params.py and add new params


This version chromedriver works with Chrome 73.0.3683. If you have a problem to run tests, check your Chrome version and download chromedriver.exe for it. Then add new chromedriver.exe to resources package.