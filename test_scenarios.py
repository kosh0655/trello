from selenium import webdriver
import pytest
import time

from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from params import *


class TestTrello():
    @pytest.fixture()
    def setup(self):
        global driver
        driver = webdriver.Chrome('resources/chromedriver.exe')
        driver.implicitly_wait(10)
        driver.maximize_window()
        driver.get('https://trello.com')
        yield
        driver.close()
        driver.quit()

    @pytest.mark.parametrize('name, email, passw, enable, url',
                             # good registration must be unique
                             [good_registration,
                              already_exists,
                              space_name,
                              long_name,
                              empty_name,
                              sql_name,
                              empty_email,
                              space_email,
                              long_email,
                              space_passw,
                              empty_passw])
    def test_registration(self, setup, name, email, passw, enable, url):
        """
        Test trello registration
        :param setup: pytest fixture before run test
        :param name: text for first input
        :param email: text for second input
        :param passw: text for third input
        :param enable: disable or not main button
        :param url: expected redirect
        """

        # Find sign up button
        driver.find_element_by_xpath('//a[@href="/signup"]').click()
        time.sleep(10)

        # Check cursor position in textbox
        # Then write test data and press TAB
        assert 'name' in driver.switch_to.active_element.get_attribute("id")
        driver.switch_to.active_element.send_keys(f'{name}')
        driver.switch_to.active_element.send_keys(Keys.TAB)

        assert 'email' in driver.switch_to.active_element.get_attribute("id")
        driver.switch_to.active_element.send_keys(f'{email}')
        driver.switch_to.active_element.send_keys(Keys.TAB)

        assert 'password' in driver.switch_to.active_element.get_attribute("id")
        driver.switch_to.active_element.send_keys(f'{passw}')

        # Check enabled of registration button, not Google
        # Click registration button if it is enabled
        assert driver.find_element_by_xpath('//input[@class="button button-green"]').is_enabled() is enable
        driver.find_element_by_xpath('//input[@class="button button-green"]').click()
        time.sleep(10)

        # Check redirect to next page
        assert f'{url}' in driver.current_url

    @pytest.mark.parametrize('user, passw, url',
                             [good_auth,
                              empty_user,
                              empty_passw_auth])
    def test_auth(self, setup, user, passw, url):
        """
        Test trello authorization
        :param setup: pytest fixture before test run
        :param user: test user
        :param passw: test password
        :param url: expected part of url
        """
        # Find login button and click
        driver.find_element_by_xpath('//a[@href="/login"]').click()
        time.sleep(10)

        # Check cursor position in textbox
        # Then write test data and press TAB
        assert 'user' in driver.switch_to.active_element.get_attribute("id")
        driver.switch_to.active_element.send_keys(f'{user}')
        driver.switch_to.active_element.send_keys(Keys.TAB)

        assert 'password' in driver.switch_to.active_element.get_attribute("id")
        driver.switch_to.active_element.send_keys(f'{passw}')
        driver.switch_to.active_element.send_keys(Keys.TAB)

        # Find authorization button and click
        driver.find_element_by_xpath('//input[@class="button button-green"]').click()
        time.sleep(10)

        # Check redirect to another page
        assert f'{url}' in driver.current_url

    def test_board(self, setup):
        """
        Test for moving card from first board to third board
        :param setup: pytest fixture before run test
        """
        # Find authorization button and login
        driver.find_element_by_xpath('//a[@href="/login"]').click()
        time.sleep(10)

        # Login
        driver.switch_to.active_element.send_keys('qwerty147874@ya.ru')
        driver.switch_to.active_element.send_keys(Keys.TAB)
        driver.switch_to.active_element.send_keys('98741ytrewq')
        driver.switch_to.active_element.send_keys(Keys.TAB)
        driver.find_element_by_xpath('//input[@class="button button-green"]').click()
        time.sleep(10)

        # If your account is new
        if 'https://trello.com/create-first-board' in driver.current_url:
            driver.find_element_by_xpath('//div[@class="first-board-navigation"]//span[5]').click()
            driver.find_element_by_xpath('//button[@class="first-board-continue-footer is-active submit-footer"]').click()
            # driver.find_element_by_xpath('//a[@class="icon-lg icon-close dark-hover js-cancel"]').click()
            time.sleep(5)
        # If your account is old
        else:
            driver.find_element_by_xpath('//li[@class="boards-page-board-section-list-item"][1]').click()
            # Find "Добавить новую карточку" on first board and click
            driver.find_element_by_xpath('//a[@class="open-card-composer js-open-card-composer"]').click()

        # Find textarea for new card and click
        title = driver.find_element_by_xpath('//textarea[@class="list-card-composer-textarea js-card-title"]')
        title.click()
        time.sleep(5)
        # Write text of new card
        title.send_keys('send answer to Alena')
        time.sleep(5)
        # Adding on board
        driver.find_element_by_xpath('//input[@class="primary confirm mod-compact js-add-card"]').click()
        time.sleep(5)

        # Count of cards on first and third boards. Need for test after moving
        backlog_count = len(driver.find_elements_by_xpath('//div[contains(@class, "js-list list-wrapper")][1]//a[contains(@class, "list-card")]'))
        ready_count = len(driver.find_elements_by_xpath('//div[contains(@class, "js-list list-wrapper")][3]//a[contains(@class, "list-card")]'))

        # Move new card from first board to third
        source_location = driver.find_element_by_xpath('//a[@class="list-card js-member-droppable active-card ui-droppable"]')
        target_location = driver.find_element_by_xpath('//div[@class="js-list list-wrapper"][3]')
        action = ActionChains(driver)
        action.drag_and_drop(source_location, target_location).perform()

        # Check result of moving
        assert len(driver.find_elements_by_xpath('//div[contains(@class, "js-list list-wrapper")][1]//a[contains(@class, "list-card")]')) == backlog_count - 1
        assert len(driver.find_elements_by_xpath('//div[contains(@class, "js-list list-wrapper")][3]//a[contains(@class, "list-card")]')) == ready_count + 1

        time.sleep(5)
